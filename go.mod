module gitlab.com/gitlab-org/docker-distribution-pruner

go 1.16

require (
	github.com/aws/aws-sdk-go v1.8.5-0.20170328201437-498eacd14c9d
	github.com/docker/distribution v2.8.1+incompatible
	github.com/docker/libtrust v0.0.0-20160708172513-aabc10ec26b7 // indirect
	github.com/dustin/go-humanize v0.0.0-20151125214831-8929fe90cee4
	github.com/go-ini/ini v1.62.0 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/hashicorp/go-multierror v1.0.0
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/opencontainers/go-digest v1.0.0 // indirect
	github.com/opencontainers/image-spec v1.0.2 // indirect
	github.com/sirupsen/logrus v1.9.0
	github.com/smartystreets/goconvey v1.6.4 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/ini.v1 v1.62.0 // indirect
	gopkg.in/yaml.v2 v2.2.8
)
